package com.topquality.velvetskin;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Diagnostica extends Activity {
    private static final String TAG = "AndroidCameraApi";
    private LinearLayout takePictureButton;
    private TextureView textureView;

    String locale;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private String cameraId;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;

    int foto_salvata;
    int count_tutorial;
    TextView nomefoto_tv;
    int nomefoto;

    int foto_count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        locale = Locale.getDefault().getLanguage();
        setContentView(R.layout.diagnostica);
        adb_chmod();
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0) {
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }
        nomefoto_tv = (TextView) findViewById(R.id.nomefoto);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        nomefoto = prefs.getInt("NOME FOTO", 0);
        int nome_foto_new = 1 + nomefoto;
        prefsEditor.putInt("NOME FOTO", nome_foto_new);
        prefsEditor.commit();
        nomefoto = prefs.getInt("NOME FOTO", 0);
        System.out.println("Nome foto " + nomefoto);
        nomefoto_tv.setText(getString(R.string.idfoto) + nomefoto + "_x.jpg");
        count_tutorial = 0;
        //tutorial();
        textureView = (TextureView) findViewById(R.id.texture);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
        takePictureButton = (LinearLayout) findViewById(R.id.btn_takepicture);
        assert takePictureButton != null;
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });
    }

    public void onDestroy() {

        super.onDestroy();

    }

    public void adb_chmod(){
        try {
        Process process = Runtime.getRuntime().exec("chmod 777 /mnt/media_rw \n exit");//adb shell \n
            BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(process.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private void tutorial() {
//        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
//         dialog.setCancelable(false);
//        ImageView immagine = (ImageView) dialog.findViewById(R.id.imageView4);
//        immagine.setBackgroundResource(R.drawable.startth1i);
//        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
//        int camera = prefs.getInt("CAMERA", 0);
//        if (camera == 0) {
//            immagine.setBackgroundResource(R.drawable.startth1i);
//        }
//
//        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
//
//        dismissButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (count_tutorial == 0) {
//                    if (locale.equals("it")) {
//                        immagine.setBackgroundResource(R.drawable.startth2);
//                    }
//                    count_tutorial = 1;
//                } else {
//                    dialog.dismiss();
//                }
//            }
//        });
//        dialog.show();
//    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open your camera here

            // Execute some code after 500 milliseconds have passed
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    openCamera();
                }
            }, 500);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };
    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onOpened(CameraDevice camera) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };
    final CameraCaptureSession.CaptureCallback captureCallbackListener = new CameraCaptureSession.CaptureCallback() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            Toast.makeText(Diagnostica.this, "Saved:" + file, Toast.LENGTH_SHORT).show();
            createCameraPreview();
        }
    };

    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void takePicture() {
        if (null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null");

            return;
        }
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Size[] jpegSizes = null;
            if (characteristics != null) {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            }
            int width = 1280;
            int height = 720;
            if (jpegSizes != null && 0 < jpegSizes.length) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }
            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<Surface>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));
            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
//            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            // Orientation
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
            //final File file = new File(Environment.getExternalStorageDirectory()+"/pic.jpg");

            /*
        String path = "/mnt/media_rw";
        Log.d("Files", "Path: " + path);
        File f = new File(path);
        if (f != null && f.listFiles().length > 0) {
            File file[] = f.listFiles();
            if(file != null && file.length > 0){
                Log.d("Files", "Size: "+ file.length);
                File test = new File(path+"/"+file[0].getName()+"/test1");
                try {
                    if (!test.exists()) {
                        test.createNewFile();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
             */

            String path = "/mnt/media_rw";
            File test = null;
            Log.d("Files", "Path: " + path);
            File f = new File(path);
            if (f != null && f.listFiles() != null && f.listFiles().length > 0) {
                File file[] = f.listFiles();
                if (file != null && file.length > 0) {
                    Log.d("Files", "Size: " + file.length);
                    test = new File(path + "/" + file[0].getName() + "/foto");
                    System.out.println("NOME CHIAVETTA = " + file[0].getName());
                    foto_salvata = 1;
                    try {
                        if (!test.exists()) {
                            test.createNewFile();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                test = new File(Environment.getExternalStorageDirectory() + "/foto");
                foto_salvata = 0;
            }
            foto_count = 1 + foto_count;
            String fotoname = "" + nomefoto + "_" + foto_count + ".jpg";
            System.out.println("NOME FOTO ----------------------> " + fotoname);
            final File file = new File(String.format(test + "/" + fotoname, System.currentTimeMillis()));
            nomefoto_tv.setText(getString(R.string.idfoto) + " " + fotoname);
            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    Image image = null;
                    try {
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);
                    } catch (FileNotFoundException e) {
                        Log.d(TAG, e.getMessage());
                    } catch (IOException e) {
                        Log.d(TAG, e.getMessage());
                    } finally {
                        if (image != null) {
                            image.close();
                        }
                    }
                }

                private void save(byte[] bytes) throws IOException {
                    OutputStream output = null;
                    try {
                        output = new FileOutputStream(file);
                        output.write(bytes);
                    } finally {
                        if (null != output) {
                            output.close();
                        }
                    }
                }
            };
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    String da_dire = null;
                    if (foto_salvata == 0) {
                        da_dire = getString(R.string.nousb);
                    } else if (foto_salvata == 1) {
                        da_dire = getString(R.string.usbok);
                    }
                    Toast.makeText(Diagnostica.this, da_dire, Toast.LENGTH_SHORT).show();
                    createCameraPreview();
                }
            };
            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        Log.d(TAG, e.getMessage());
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(Diagnostica.this, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openCamera() {
        Log.e(TAG, "is camera open");
        try {
            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            if (manager.getCameraIdList() != null && manager.getCameraIdList().length <= 0) {
                camera_non_collegata();
            } else {
                try {
                    if (manager.getCameraIdList() != null && manager.getCameraIdList().length > 0) {
                        if (manager.getCameraIdList()[0] != null) {
                            cameraId = manager.getCameraIdList()[0];
                            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                            assert map != null;
                            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
                            // Add permission for camera and let user grant the permission
                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(Diagnostica.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                                return;
                            }
                            manager.openCamera(cameraId, stateCallback, null);
                        }
                    }
                } catch (CameraAccessException e) {
                    Log.d(TAG, e.getMessage());
                }
                Log.e(TAG, "openCamera X");
            }
        } catch (CameraAccessException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    public void camera_non_collegata() {
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.no_camera);
        dialog.setCancelable(false);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog.dismiss();

            }
        });
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void updatePreview() {
        if (null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return");
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(Diagnostica.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    /*funzioni fisse per tutti */
    public void back(View view) {

        //onDestroy();
        finish();
    }

//    public void informazioni(View view) {
//       // onDestroy();
//        finish();
//        Context context = getBaseContext();
//        Intent CauseNelleVic = new Intent(context, Informazioni.class);
//        startActivityForResult(CauseNelleVic, 0);
//    }
//
//    public void home(View view) {
//        Context context = getBaseContext();
//        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
//        startActivityForResult(CauseNelleVic, 0);
//
//        finish();
//    }
}